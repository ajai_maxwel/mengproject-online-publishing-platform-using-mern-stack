var express = require('express')
var app = express()
const passport = require('passport')
var Promise = require('bluebird');
const _ = require('lodash')
const keys = require('./config/keys')
//for mail
const SendMail = require('./Service/sendMail')

const GOOGLE_CLIENT_ID = keys.GOOGLE_CLIENT_ID
const GOOGLE_CLIENT_SECRET= keys.GOOGLE_CLIENT_SECRET

var GoogleStrategy = require('passport-google-oauth20').Strategy;
var myData = []


const cookieSession = require('cookie-session')
const bodyParser = require('body-parser')


app.use(bodyParser.json({limit: '50mb'}))
app.use(express.urlencoded({limit: '50mb'}));
app.use(bodyParser.text())
app.use( cookieSession({
    maxAge: 30*24*60*60*1000,       //milliseconds
    keys: [keys.COOKIE_KEY],
}))
const findOrCreate = require('mongoose-findorcreate')
const mongoose = require('mongoose')
mongoose.connect(keys.MONGO_URI)

require('./models/user')
require('./models/article')
require('./models/topic')
require('./models/mail')
require('./models/reportedUsers')
require('./models/bannedUsers')

const Users = mongoose.model('users')
const Articles = mongoose.model('articles')
const Topics = mongoose.model('topics')
const Reports = mongoose.model('reports')
const BannedUsers = mongoose.model('bannedUsers')
//const RequireLogin = require('./Service/validateLogin')

require('./routes/adminRoutes')(app)

// used to serialize the user for the session
passport.serializeUser((user, done)=> {
//    console.log("controll in serialize")
    done(null, user.id);
});

// used to deserialize the user
passport.deserializeUser((id,done)=>{
//    console.log("controll in deserialize")
    Users.findById(id).then(user => {
        done(null,user)
    })
})

passport.use(new GoogleStrategy({

        clientID: GOOGLE_CLIENT_ID,
        clientSecret: GOOGLE_CLIENT_SECRET,
        callbackURL: "/oauth/google/callback"
    },
    async (accessToken, refreshToken, profile, done) => {
        //console.log("profile", profile)
        const banned_user = await Users.findOne({googleID: profile.id}, 'status')
        console.log("user status: ", banned_user)

        const old_user = await Users.findOne({googleID: profile.id})
        if (old_user) {
            //do not add update the DB
            //console.log("old user!!!!")
            //Users.update({googleID: profile.id},{email: profile.emails[0].value, image: profile.image,}).exec()
            if(!banned_user.status){
                //console.log(profile.id + "is banned")
                done(null,banned_user)
            }else
                done(null, old_user)
        }
        else {
            const new_user = await new Users({
                googleID: profile.id,
                name: profile.displayName,
                email: profile.emails[0].value,
                image: profile.image,
            }).save()
            done(null, new_user)
        }
    }
));
app.use(passport.initialize())
app.use(passport.session())

app.get('/oauth/google',
    passport.authenticate('google', { scope: ['profile', 'email']}));

app.get('/oauth/google/callback',
    passport.authenticate('google'),
    (req, res) => {
        // Successful authentication, redirect home.
        console.log('passport: ',req)
        if (!req.user.status)
            res.redirect('/banPage')
        else
            res.redirect('/');
    });

app.get('/api/current_user', (req,res) => {
//    console.log("making current user request", req.user)
    res.send(req.user)
})

app.get('/api/logout',(req,res)=>{
    req.logout()
    res.redirect('/')

})

app.get('/api/post', async (req,res)=> {
    const results = await Articles.find({})
//    console.log("full post content results: ", results)
    res.send(results)
})


//new post
app.post('/api/post/new', async (req,res) =>{
    console.log("post_content", req.body)
    const {writtenContent, writtenTopic} = req.body

    const new_post = new Articles({
        postContent:writtenContent,
        postedBy:req.user.name
    })
//    console.log("new", new_post)
    await new_post.save()
    //code to check and save if the topic is new in Topics-doc
    //const updateTopic = await Topics.findOrCreate({topicName:writtenTopic})
    const findTopic = await Topics.findOne({topicName:writtenTopic},'topicName')
    console.log('findTOpic: ',findTopic)
    if(findTopic===null){
        console.log('findTOpic in to if condition: ', findTopic)
        const newTopic = new Topics({topicName:writtenTopic})   //Create new row in topic table.
        await newTopic.save()
    }

    //Code to send mails to subscribed users.
    const mailRecipients = await Topics.findOne({topicName:writtenTopic}, 'mailList') //returns the mailList array.
    if(mailRecipients.mailList.length != 0){
        const postID = await Articles.findOne({postContent:writtenContent},'._id') //returns post ID.
        console.log('Mail1: keys and reci: ',postID, mailRecipients) //postID._id is obtained.
        const sendMail = new SendMail(mailRecipients, postID, writtenTopic) //"react shd be made dynamic based on what user post"
        await sendMail.send()
    }

    res.send(req.user)

})
//test:
app.post('/test01', async (req,res) => {
    const mailRecipients = await Topics.findOne({topicName:'React'}, 'mailList') //returns the mailList array.
    const postID = { _id: '5ab0c51dc9e0ad38546c9c22' }
    const sendMail = new SendMail(mailRecipients, postID, 'React')
    await sendMail.send()
    res.send('DONE!')
})

app.post('/api/pin', async (req,res) => {
//   console.log('control inside backend pin', req.body)
//    console.log('control inside backend pin, rquest user is ', req.user)
   const {topicInterested} = req.body._id
   //const results = Users.updateOne({ topicInterested})
   //if topicInterested is null, topicInterested = req.body._id
   //if already content pined then,  req.user.topicInterested += "," + req.body._id
    if(req.user.PinnedPost){
       req.user.PinnedPost += "," + req.body._id
   }else{
        req.user.PinnedPost = req.body._id
    }
    //req.user.topicInterested += "," + req.body._id//need to be changed for new user
   await req.user.save()
    //res.send()
})

getPinnedArticle = (pins) => {
            return new Promise(function (fullfill, reject) {

                final = pins.split(',').map(async pin => {
                    await Articles.find({_id:pin.trim()},'postContent')


                 })
                fullfill(final)

            })
   //sendData = await final()

    //console.log('send data is: ', final)

}

app.get('/api/home/pins',async (req,res)=>{
    pins = await req.user.PinnedPost
    var promises = pins.split(',').map(async pin => {
//        console.log('pin trims is: ', pin.trim())
        return await Articles.find({_id:pin.trim()},'postContent')
    })
//    console.log('send data is: ', promises)
    Promise.all(promises).then(function (results) {
//        console.log('result is: ', results)
        res.send(_.flattenDeep(results))
    })


})

app.get('/api/home/post/view', async (req,res)=> {
    //const results = await Articles.find({})
    const str = req.url.substring(req.url.length - 24)   //change the substring to more flexible
//    console.log("string is : ", str)
    const result = await Articles.find({_id:str})

   console.log("result passed to view content is : ", result)




    res.send(result)
})


//1.get searchURL
//2.Slice it to get the search string.
//3.make request to articleDB with the search string.
//4.Fetch the contents.
//5.return the fetched contents.
//'/api/home/search
//http://localhost:3000/api/home/search?ID=ffmmm 404


app.get('/api/home/search', async (req,res)=> {
    //console.log('the search string is: ', req.url)   //the search string is:  /api/home/search?ID=katrina+Kaif
    const searchString = req.url.split('=')[1]         //katrina+Kaif
    //console.log('the search string is: ', searchString)
    const SearchResults = await Articles.find({$text:{$search:searchString }})   //{postContent:searchString},'postContent'
    //console.log('the search Results are :', SearchResults)
    res.send(SearchResults)


})
//'/api/like'
app.post('/api/like',async (req,res) => {
    //console.log("control passed to api/like",req.body)  //req.body = {postID: '5a7c386312f78b21e8572ad5' }
    const {postID, likeCount} = req.body
    const postLike = Articles.findByIdAndUpdate(req.body.postID, { $inc: { likes: 1 }} )
    await postLike.exec()
    const getPost = await Articles.find({_id:postID}).exec()
    res.send(getPost)
})

//add new topic to the topic-Table.
app.post('/api/topic/new', async (req,res) => {
    const newTopic = new Topics({topicName:'Angular5'})   //Create new row in topic table.
    await newTopic.save()
    res.send('obj posted!!')

})
//fetch topics from the topic-table.
app.get('/api/topic/get', async (req,res) => {
    const results = await Topics.find({})
    res.send(results)
})

//api/interest/post
app.post('/api/interest/post', async (req,res) => {
    //console.log('Profile: bE',req.body)
    const topicInterested = req.body
    //console.log('llaa', {topicInterested})
    const interest = Users.update({ _id: req.user._id },{topicInterested:topicInterested})
    await interest.exec()
    //Delete all entries related to UserID in TopicsTable.
    //const deleteMail = Topics.deleteMany({emailList:{userID: req.user._id}}).exec()

    const deleteMail = await Topics.update(
        { },
        { $pull: { mailList: {mail: req.user.email} } },
        { multi: true }
    ).exec()
    //send: UserID mailID TopicID
    //fetchQuery: based on TopicID
    const mailupdate = topicInterested.map(async topic => {
        Topics.update(
            { _id: topic },
            { $push: {mailList:{mail:req.user.email, userID:req.user._id}} }
        ).exec()
        //console.log('TP: topic table entries: ',mailList)
    })
    //await mailupdate.save()
    //console.log('DB: user interest: ',req.user.topicInterested)
    res.send('obj posted!!')

})
//post report.
app.post('/api/report/',async (req,res) => {
    console.log('report:', req.body)
    const {postID} = req.body
    const getAuthor =  await Articles.findOne({_id:postID}, 'postedBy postContent')
    console.log('get author:',getAuthor)
    const getAuthorName = await Users.findOne({name:getAuthor.postedBy}, 'googleID')

    const report = new Reports({
        googleID: getAuthorName.googleID,
        name: getAuthor.postedBy,
        articleReportedID: postID,
        articleReportedContent: getAuthor.postContent,
        votes:1,
    })
    await report.save()
    //res.send(report)
})

//fetch reported user list from the report-table.
app.get('/api/admin/reports', async (req,res) => {
    const results = await Reports.find({})
    res.send(results)
})


//'/api/admin/banUser'
app.post('/api/admin/banUser',async (req,res) => {
    console.log('body in /api/admin/banUser is ', req.body)
    const {userID} = req.body
    const status = await Users.findOne({ googleID: userID }, 'status').exec()
    const ban = Users.update({ googleID: userID },{status:!status.status })
    await ban.exec()
    const banFlag = await Reports.findOne({googleID:userID},'banFlag').exec()
    const setFlag = Reports.update({googleID:userID},{banFlag:!banFlag.banFlag })
    await setFlag.exec()
    console.log('the flags are ',status,banFlag)
    const results = await Reports.find({})
    res.send(results)
})
app.get('/api/current_admin', (req,res) => {
   console.log("making current admin request", req.user)
    res.send(req.user)
})
///api/comment/post
//post comments.
app.post('/api/comment/post', async (req,res)=>{
    const {comment, postID} = req.body
    const date = Date.now()
    await Articles.update(
        { _id: postID },
        { $push: {comments:{comment:comment, postedBy:req.user.name, userID:req.user._id, postedOn:date}} }
    ).exec()
    const getArticle = await Articles.find( { _id: postID }).exec()
    //await getArticle.exec()
    res.send(getArticle)

})
//for testing: retrieve comment:
app.get('/api/comment/post', (req,res)=>{

})
const PORT = process.env.PORT || 8080
app.listen(PORT)
