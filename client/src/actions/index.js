import axios from 'axios'

//action types
export const GET_USER = 'GET_USER'
export const GET_POSTS = 'GET_POSTS'
export const OPEN_CONTENT = 'OPEN_CONTENT'
export const SEARCH_RESULTS = 'SEARCH_RESULTS'
export const GET_PINS = 'GET_PINS'
export const GET_RESPONSE = 'GET_RESPONSE'
export const GET_TOPICS = 'GET_TOPICS'
export const GET_REPORTS = 'GET_REPORTS'
export const GET_ADMIN = 'GET_ADMIN'
export const GET_COMMENT = 'GET_COMMENT'


//action handler:  get_User: call to get current user
export const get_User = () => async dispatch => {
    const res = await axios.get('/api/current_user')
    dispatch({type: GET_USER, payload: res.data })
}

//action handler: post_article:  call to post new article. Values contain article content.
export const post_article = (values, history) => async (dispatch) => {
    //console.log("control passed to post_article", values)
    const res = await axios.post('/api/post/new',values)
    dispatch({type: GET_USER, payload: res.data })
}

//action handler: get_Posts: call to get all the posts from article document.
export const get_Posts = () => async dispatch => {
    const res = await axios.get('/api/post')
    dispatch({type: GET_POSTS, payload: res.data })
}

//action handler: pin_Post: values contain article postID
export const pin_Post = (values) => async dispatch => {
    const pinID = values._id
    //console.log('pinned post id is: ',pinID)
    const res = await  axios.post('/api/pin',values)
    dispatch({type: GET_USER, payload: res.data })
}


//action handler: get_pins:
export const get_Pins = () => async dispatch => {
    console.log("controll passed to get pins frontend")
    const res = await axios.get('/api/home/pins')
    dispatch({type: GET_PINS, payload: res.data })
}

//get_PostonID NOT USED
//this action handler gets a post corresponding to the passed values. ie..post_ID
export const get_PostonID = (values) => async dispatch => {
    //console.log("controll passed to get get_PostonID frontend",values)
    const res = await axios.get('/api/postID', {params:{ID:values}})
    dispatch({type: GET_RESPONSE, payload: res.data })
}

//action handler view_content: values contain postID
export const view_Content = (values) => async dispatch => {
    const res = await axios.get('/api/home/post/view', {params:{ID:values}})
    dispatch({type: OPEN_CONTENT, payload: res.data })
}

//for getting search result: values contain search string
export const request_Search = (values) => async dispatch => {
    const res = await axios.get('/api/home/search', {params:{ID:values}})
    dispatch({type: SEARCH_RESULTS, payload: res.data})
}

//post_Likes: values contain current like count
export const post_Likes = (values) => async dispatch => {
    const res = await axios.post('/api/like',values)
    dispatch({type: OPEN_CONTENT, payload: res.data })
}

//get data from topics table.
export const get_Topic = () => async dispatch => {
    //console.log("control passed to get topic frontend")
    const res = await axios.get('/api/topic/get')
    dispatch({type: GET_TOPICS, payload: res.data})
}

//post_interest: values contain topicID
export const post_Interest = (values) => async dispatch => {
    //console.log("Profile: control passed FE ....",values)
    const res = await axios.post('/api/interest/post',values)
    dispatch({type: GET_USER, payload: res.data })
}


//post_Report: values contain postID
export const post_Report = (values) => async dispatch => {
    //console.log('report: the values passed is: ', values)
    const res = await axios.post('/api/report/',values)
    dispatch({type:  GET_TOPICS, payload: res.data })
}


//FETCH REPORTS for admin.
export const get_Reports = () => async dispatch => {
    const res = await axios.get('/api/admin/reports')
    dispatch({type: GET_REPORTS, payload: res.data})
}

//ban user, change status to "ban" in UserDB.
export const ban_User = (values) => async dispatch => {
    //console.log('ban: the values passed is: ', values)
    const res = await axios.post('/api/admin/banUser',values)
    dispatch({type: GET_REPORTS, payload: res.data })
}

//values contain username and password from admin login.
export const admin_Validate = (values) => async dispatch => {
    //console.log('FE:the values passed in admin validate is: ', values)
    const res = await axios.post('/api/admin/validate',values)
    dispatch({type: GET_ADMIN, payload: res.data })
}


export const get_Admin = () => async dispatch => {
    const res = await axios.get('/api/current_admin')
    dispatch({type: GET_ADMIN, payload: res.data })
}

//delete post. value contain postID
export const delete_Post = (values) => async dispatch => {
    //console.log('FE:the values passed in delete post is is: ', values)
    const res = await axios.post('/api/admin/delete/post',values)
    dispatch({type:  GET_REPORTS, payload: res.data })
}
//delete report document. values contain reportID
export const delete_Report = (values) => async dispatch => {
    //console.log('FE:the values passed in delete report is is: ', values)
    const res = await axios.post('/api/admin/delete/report',values)
    dispatch({type:  GET_REPORTS, payload: res.data })
}

//post new comment. values contains comment data and postID
export const post_Comment = (values) => async dispatch => {
    //console.log('FE:the values passed in post comment is: ', values)
    const res = await axios.post('/api/comment/post',values)
    dispatch({type:  OPEN_CONTENT, payload: res.data })
}