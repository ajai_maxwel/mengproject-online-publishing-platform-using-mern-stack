import {GET_TOPICS} from '../actions'

export default function (state= [], action) {
    console.log('actions: ',action)
    switch (action.type){
        case GET_TOPICS:
            return action.payload
        default:
            return state
    }
}