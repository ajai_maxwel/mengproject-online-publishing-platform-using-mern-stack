import {GET_REPORTS} from '../actions'

export default function (state= [], action) {
    console.log('actions: ',action)
    switch (action.type){
        case GET_REPORTS:
            return action.payload
        default:
            return state
    }
}