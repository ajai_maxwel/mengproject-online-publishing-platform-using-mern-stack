import {GET_RESPONSE} from '../actions'

export default function (state= [], action) {
    console.log('actions: ',action)
    switch (action.type){
        case GET_RESPONSE:
            return action.payload
        default:
            return state
    }
}