import {SEARCH_RESULTS} from '../actions'

export default function (state= [], action) {
    console.log('actions: ',action)
    switch (action.type){
        case SEARCH_RESULTS:
            return action.payload
        default:
            return state
    }
}