import {GET_ADMIN} from '../actions'

export default function (state= {}, action) {
    console.log('actions: ',action)
    switch (action.type){
        case GET_ADMIN:
            return action.payload
        default:
            return state
    }
}