import {GET_USER} from '../actions'

export default function (state= {}, action) {
    console.log('actions: ',action)
    switch (action.type){
        case GET_USER:
            return action.payload
        default:
            return state
    }
}