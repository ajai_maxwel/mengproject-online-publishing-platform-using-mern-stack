import {GET_COMMENT} from '../actions'

export default function (state= [], action) {
    console.log('actions: ',action)
    switch (action.type){
        case GET_COMMENT:
            return action.payload
        default:
            return state
    }
}