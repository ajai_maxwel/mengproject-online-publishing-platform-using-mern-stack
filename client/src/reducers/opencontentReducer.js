import {OPEN_CONTENT} from '../actions'

export default function (state= [], action) {
    console.log('actions: ',action)
    switch (action.type){
        case OPEN_CONTENT:
            return action.payload
        default:
            return state
    }
}