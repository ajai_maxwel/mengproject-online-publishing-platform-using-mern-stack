import {GET_PINS} from '../actions'

export default function (state= [], action) {
    console.log('actions: ',action)
    switch (action.type){
        case GET_PINS:
            return action.payload
        default:
            return state
    }
}