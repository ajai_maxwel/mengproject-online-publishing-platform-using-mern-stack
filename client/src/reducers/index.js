import {combineReducers} from 'redux'
import { reducer as formReducer } from 'redux-form'
import authReducer from './authReducer'
import postReducer from './postsReducer'
import opencontentReducer from './opencontentReducer'
import searchReducer from "./searchReducer";
import pinReducer from "./pinReducer";
import responseReducer from "./responseReducer";
import topicsReducer from "./topicsReducer"
import reportsReducer from "./reportsReducer";
import adminReducer from "./adminReducer";
import commentReducer from "./commentReducer";

export default combineReducers({
        auth: authReducer,
        post: postReducer,
        open: opencontentReducer,
        form: formReducer,
        search: searchReducer,
        pin: pinReducer,
        response:responseReducer,
        topics: topicsReducer,
        reports: reportsReducer,
        admin: adminReducer,
        comment:commentReducer,
    }

)