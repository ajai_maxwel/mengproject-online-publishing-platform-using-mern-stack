import React, { Component } from 'react';
import {BrowserRouter, Route } from 'react-router-dom'
import Header from './Header'
import {connect} from 'react-redux'
import * as actions from '../actions'
import NewPost from './NewPost'
import Dashboard from './Dashboard'
import {withRouter} from 'react-router-dom'
import Home from './Home'
import ViewContent from './ViewContent'
import WrongPath from './WrongPath'
import SearchBar from './SearchBar'
import SearchResults from './SearchResults'
import MyProfile from './MyProfile'
import AdminPortal from './AdminPortal'
import BanPage from './BanPage'
import AdminLogin from "./AdminLogin";
import ShowReport from "./Forms/ShowReport";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
          <div>

              <Header />
              <Route exact path = "/dashboard" component={Dashboard} />
              <Route exact path = "/home/post/new" component={NewPost} />
              <Route exact path = "/home" component = {Home}/>
              <Route exact path = "/post/:id" component = {ViewContent}/>
              <Route exact path = "/post/www*" component={WrongPath} />
              <Route exact path = "/search" component={SearchBar}/>
              <Route exact path = "/search" component={SearchResults}/>
              <Route exact path = "/myProfile" component={MyProfile}/>
              <Route exact path = "/banPage"component={BanPage} />
              <Route exact path = "/admin/portal" component={AdminPortal} />
              <Route exact path = "/admin/" component={AdminLogin} />
              <Route exact path = "/admin/view/report" component={ShowReport}/>

          </div>



      </BrowserRouter>

    );
  }
}

export default App;
//export default connect(null)(App)