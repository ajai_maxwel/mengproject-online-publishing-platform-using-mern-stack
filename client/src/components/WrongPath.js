import React, {Component} from 'react'

class WrongPath extends Component{
    render(){
        return(
            <div className='container'>
                <div>
                    <strong>Path Not Found!!!!</strong>
                </div>
                <div>
                    <small>check if the path begins with "https://"</small>
                </div>

            </div>
        )
    }
}

export default WrongPath