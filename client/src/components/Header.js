import React, {Component} from 'react'
import {connect} from 'react-redux'
import {get_User} from '../actions'
import * as actions from '../actions'
import {Link} from 'react-router-dom'
import BanPage from "./BanPage";
import AdminLogin from './AdminLogin'

class Header extends Component{
    componentDidMount(){
        this.props.get_User()
        console.log("this.props.getuser", this.props.auth)
    }

    renderContent(){

        //console.log('history is: ',this.props)

        if(window.location.href==="http://localhost:3000/admin")
            return <div>
                <div>Hello Admin!</div>
            </div>


        if(!this.props.auth){
            return [

                        <li>
                            <a href='/oauth/google'>Sign in with Google</a>

                        </li>,



            ]
        }else{

            if(!this.props.auth.status)
                return <div>Access Denied!</div>

            return [
                <li>Hello {this.props.auth.name} </li>,

                <li className="btn-flat black" style={{margin: '10px 10px'}}>
                    <Link to="/home/post/new">New Post</Link>

                </li>,

                <li className="btn-flat black" style={{margin: '10px 10px'}}>
                     <Link to="/home">My Board</Link>
                </li>,

                <li className="btn-flat black left" style={{margin: '10px 10px'}}>
                    <Link to="/dashboard">Main page</Link>
                    </li>,

                <li className="btn-flat black left" style={{margin: '10px 10px'}}>
                    <a href="/myProfile">MyAccount</a>
                </li>,

                <li className="btn-flat black left" style={{margin: '10px 10px'}}>
                    <Link to="/search">Search</Link>
                </li>,

                <li className="btn-flat black left" style={{margin: '10px 10px'}}>
                    <a href='/api/logout'>LogOut</a>
                </li>
                ]
        }

    }
    render(){
        return(
            <div className="">

                <nav>
                    <div className="nav-wrapper grey">
                        <ul id="nav-mobile" className="left-align">
                        {this.renderContent()}
                        </ul>
                    </div>
                </nav>
            </div>
        )
    }
}
function mapStateToProps({auth}) {
    return {auth}
}
export default connect(mapStateToProps,actions)(Header)