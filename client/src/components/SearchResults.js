/*
*Component: SearchResults
* 1.Set an action handler to retrieve results from Store.
* 2.Display the result.
* Feature:
* Pagination concept is used to display the search results.
* Currently displays 10 results per page
* On max/min page limit reached, the next/prev buttons are disabled
*
* */

import React, {Component} from 'react'
import {connect} from 'react-redux'
import {request_Search} from "../actions";


class SearchResults extends Component{
    constructor(){
        super()
        this.state = {a:0,b:10, maxSearchFlag:false, minSearchFlag:false}
        //(a,b) are the parameters passed to array.slice(start, end)
        //min/maxSearchFlag are set when the end limit is reached, so that we can disable the button
        this.onPrevClick = this.onPrevClick.bind(this)
        this.onNextClick = this.onNextClick.bind(this)
    }

    componentDidMount(){
        this.props.request_Search
        //fetch the search results from store.
    }

    //each page is designed to have 10 search result.
    onClick = (e,i) => {
        e.preventDefault()
        console.log('Search: onclick',i)
        const end = i*10
        const start = end - 10
        this.setState({a:start,b:end,maxSearchFlag:false,minSearchFlag:false}, (a,b)=> console.log(a,b))
    }
    //compute for the previous 10 post to be rendered.
    onPrevClick(e){
        e.preventDefault()
        const end = this.state.b - 10
        const start = this.state.a - 10
        console.log('Search: onPrevClick',start,end)
        if(start >= 0){
            this.setState({a:start,b:end,maxSearchFlag:false}, (a,b)=> console.log(a,b))
        }
        this.setState({minSearchFlag:true})
    }
    //compute the next 10 post to be rendered.
    onNextClick(e){
        e.preventDefault()
        const end = this.state.b + 10
        const start = this.state.a + 10
        const endPage = this.props.Post.length
        console.log('Search: onPrevClick',start,end)
        if(start <= endPage){
            this.setState({a:start,b:end,minSearchFlag:false}, (a,b)=> console.log(a,b))
        }
        this.setState({maxSearchFlag:true})
    }

    //creates a list of buttons for pagination.
    createButtonList(){
        const resultCount = this.props.Post.length
        let buttonList = []  //an array to hold the buttons.
        console.log('Search: inside Display')
        const pages = Math.ceil(resultCount / 10)
        if(resultCount){
            buttonList.push(<button className="waves-effect" disabled={this.state.minSearchFlag} onClick={this.onPrevClick}>Prev</button>)
            for (let i = 1; i <= pages; i++) {
                buttonList.push(<button className="waves-effect" onClick={(e)=>this.onClick(e,i)}>{i}</button>)
            }
            buttonList.push(<button className="waves-effect" disabled={this.state.maxSearchFlag} onClick={this.onNextClick}>Next</button>)
            console.log('Search loop: ', buttonList)
        }
        return buttonList
    }
    //createButtonList can be directly called from render.
    createPagination = () =>{
        console.log('Search: control n createPagination')
        return(
            <ul className="pagination">
                {this.createButtonList()}
            </ul>
        )
    }


    renderSearch(){
        const {a,b} = this.state
        return this.props.Post.slice(a,b).map(post => {

            console.log("Search: inside renderSearch array: ",this.props.Post.length, this.props.Post.slice(a,b))

            return(
                <div  key={post._id} >
                    <a href={`/post/${post._id}`}  target="_blank">
                    <div className="card white darken-2 hoverable">
                        <div className="card-content" >
                                <li dangerouslySetInnerHTML = {{ __html: post.postContent }} />
                        </div>
                    </div>
                    </a>
                </div>

            )
        })
    }

    maxSearchReached = ()=> {
        console.log('Search: rahuldravid')
        if(this.state.maxSearchFlag){
            return( <div className="materialize-red-text"><small>***max search reached!!</small> </div>)

        }
        return

    }

    render(){

        return(
            <div className="container">
                <div style={{margin: '10px 10px'}}>Results: {this.props.Post.length}</div>
                {this.maxSearchReached()}
                {this.createPagination()}
                <div>
                    <ul>
                        {this.renderSearch()}  {/*a function to render based on a,b*/}
                    </ul>
                </div>


            </div>
        )
    }
}

function mapStateToProps(state){
    console.log("state_post is ",state.search)
    return {Post: state.search}  //from reducer
}

export default connect(mapStateToProps,{request_Search})(SearchResults)