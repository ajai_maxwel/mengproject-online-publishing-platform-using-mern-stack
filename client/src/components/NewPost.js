import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import {Field, reduxForm} from 'redux-form'
import {connect} from 'react-redux'
import {withRouter} from 'react-router-dom'

import {post_article} from '../actions'
import * as actions from '../actions'
import NewPostForm from './Forms/NewPostForm'
import MaxEditor from './MaxEditor'

class NewPost extends Component{
       submit = (values) => {
        //let {history} = this.props
        //console.log('history',history)
        this.props.post_article(values)
        this.props.history.push('/home')
        //console.log('history',history)

    }
    constructor(){
        super()
        this.state = {showFlag:false}

    }
    componentWillMount(props){
        this.props.get_Topic()
        this.callEditor = this.callEditor.bind(this)
    }
    renderTopics(){
        return this.props.Topics.map(topic => {
            return(
                topic.topicName
            )
        })
    }
    callEditor(totalTopics){
        if(totalTopics.length){
            //this.setState({showFlag:true})
            console.log('test1 totalTopics n callEditor is',{totalTopics})
            return(
                <MaxEditor totalTopics = {totalTopics} />
            )

        }
    }
    render(){
        const totalTopics = this.renderTopics()
        console.log('test1 in newpost comp',{totalTopics})
        return(
            <div className="container">
                <div className="input-field col s6">
                    {/* <NewPostForm onSubmit={this.submit.bind(this)} />*/}
                    {this.callEditor(totalTopics)}
                </div>
            </div>
        )
    }
}
function mapStateToProps(state) {
    console.log('state: ',state)
    return {state, Topics: state.topics }

}

export default connect(mapStateToProps, actions)(withRouter(NewPost))
//export default withRouter(connect(mapStateToProps, actions)(NewPost))