import React, {Component} from 'react'
import {connect} from 'react-redux'
//import {get_User, get_Topic, post_Interest} from '../actions'
import * as actions from '../actions'
//import {Link} from 'react-router-dom'

import { Field, reduxForm } from 'redux-form'
//import Multiselect from 'react-widgets/lib/Multiselect'
import { Multiselect } from 'react-widgets'
import PropTypes from 'prop-types';
import 'react-widgets/dist/css/react-widgets.css'

/*
* Display users Profile. 1.Name, 2. Topic interested. 3. His articles. 3.Edit button. 4.Subscribe button.
* onClick Edit should enable user to change his topic-interest and Subscribe button.
* */
class MyProfile extends Component{

    constructor(...props){
        super(...props)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.renderInitialTopic = this.renderInitialTopic.bind(this)
        this.state = {
            value: this.props.auth.topicInterested,
            onEditFlag: false
        }
    }

    //calling the action handler to fetch data from topic table.
    componentWillMount(props){
        this.props.get_Topic()
        this.renderInitialTopic()
    }


    //returns the list of topics.
    renderTopics(){
        return this.props.Topics.map(topic => {
            return(
                topic.topicName
            )
        })
    }
    // Previous setting for user interest.
    renderInitialTopic(flag){
        console.log('Profile1: control in inital render', flag)
       /* return(
            !this.props.data?
                'loading...'
                :
                this.setState({value : this.props.auth.topicInterested},(value)=>console.log('Profile: componentDidMount: ',value))
        )
       */

        let initialInterest= this.props.auth.topicInterested//Id only.
        console.log('Profile1: prev interest: ',initialInterest)
        if(initialInterest){
            let showitem = initialInterest.split(",").map(anInterest => {
                //console.log('Profile1: an interest: ',anInterest)
                let len = this.props.Topics.length
                if(this.props.Topics){
                    for(var i=0; i < len; i++){
                    let topic  = this.props.Topics[i]
                    if(topic._id === anInterest){
                        console.log('Profile1: topic._id: ',topic, flag)
                        if(flag){
                            console.log('Profile1: flag suces')
                            return (topic.topicName)

                        }


                        if(flag=='')
                            return(<li>{topic.topicName} ,</li>)
                    }

                }}
                console.log('Profile1 final is: ', this.final)
            })
            return showitem
        }

        //console.log('Profile: prev interest: ',this.props.auth.topicInterested)
        //this.setState({value : initialInterest})
        return initialInterest
    }

    renderMultiselect = ({ input, data, valueField, textField }) =>
        <Multiselect {...input}
                     onBlur={() => input.onBlur()}
                     //value = {input.value || this.props.auth.topicInterested}  //value={input.value || ['hello', 'maxwel']} requires value to be an array
                     value = {this.state.value || this.renderInitialTopic({flag:'multi'})} //can set the initial value that appears on the field.
                     data={data}
                     valueField={valueField}
                     textField={textField}
                     onChange={ value => this.setState({ value })}

        />

    //handle submit.
    handleSubmit(e,value){
        e.preventDefault()
        let options =this.state.value
        //return the ID of the selected topic.
        //console.log('Profile: length', this.props.Topics.length )
        let len = this.props.Topics.length
        //if(this.state.value){}
        let final = options.map(option =>{
             for(var i=0; i< len; i++){
                 let topic = this.props.Topics[i]
                 if(option=== topic.topicName){
                    console.log('Profile: topic._id: ',option)
                    return topic._id
                 }
             }
        })
        this.setState({onEditFlag:false})
        console.log('Profile: userInterest:options: ',final)
        this.props.post_Interest(final)
    }
    handlechange(e){
        console.log('Profile: change is: ', e.value)
    }

    topicForm = (props) => {
        //const {handleSubmit} = props
        if(this.state.onEditFlag)
        return(
            <form onSubmit={this.handleSubmit}>
                <div className="">
                    <label>Choose your interested topics</label>
                    <Field name="topicList"
                           component={this.renderMultiselect}
                           data={this.renderTopics()} ref='ti'
                    />
                </div>

                <button type="submit" >Done</button>
                <button type="submit" onMouseDown={(e)=>{this.setState({value: this.renderInitialTopic({flag:''}), onEditFlag:false })}}>Cancel</button>


            </form>
        )
    }

    render(){
        const london = this.props.auth.topicInterested


        return(
            <div className='container color lighten-1' >

                <ul>
                    <div className='card-title color '><li>Name : {this.props.auth.name}</li> </div>
                    <div><li>Topics Subscribed :
                        {this.state.value ||this.renderInitialTopic({flag:''}) }
                    </li></div>
                    <div><li>Send email : {this.props.auth.email}</li></div>
                </ul>
                <button type="button" onMouseDown={(e)=> {this.setState({onEditFlag:true})} }>Edit</button>
                {this.props.handleSubmit}
                <div className="">{this.topicForm()}</div>

            </div>
        )
    }
}
function mapStateToProps(state){
    console.log("Profile: state.topics is ",state.topics)
    return {Topics: state.topics, auth: state.auth}  //from reducer
}

MyProfile = reduxForm({
    form: 'topicForm'  // a unique identifier for this form
})(MyProfile)


export default connect(mapStateToProps,actions)(MyProfile)