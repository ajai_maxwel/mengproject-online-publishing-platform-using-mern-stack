import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import {Editor, EditorState, RichUtils, CompositeDecorator, getDefaultKeyBinding,AtomicBlockUtils, Entity} from 'draft-js';
import {stateToHTML} from 'draft-js-export-html';

//style for react-widget, dropdown.
import 'react-widgets/dist/css/react-widgets.css'

import {post_article} from '../actions'
import * as actions from '../actions'
import {withRouter} from 'react-router-dom'
import materializeCSS  from 'materialize-css/dist/css/materialize.min.css'

import DropdownList from 'react-widgets/lib/DropdownList'
import SelectList from 'react-widgets/lib/SelectList'

//import 'draft-js/dist/Draft'

const colors = [ { color: 'React', value: 'ff0000' },
    { color: 'Green', value: '00ff00' },
    { color: 'Blue', value: '0000ff' } ]



let colorValue = []
let displayTopics = []
let newTopic = []
class MaxEditor extends Component{
    constructor(props) {
        super(props);
        this._onSingleLineStyle = this._onSingleLineStyle.bind(this)
        this._onMultiBlockStyle = this._onMultiBlockStyle.bind(this)
        this._onLinkClick = this._onLinkClick.bind(this)
        this._onURLChange = this._onURLChange.bind(this)
        this._onConfirmURLClick = this._onConfirmURLClick.bind(this)
        this._onCancelURLClick = this._onCancelURLClick.bind(this)
        this._onKeyBoardCommand = this._onKeyBoardCommand.bind(this)
        this._onEnterKeyDown = this._onEnterKeyDown.bind(this)
        this._onImageClick = this._onImageClick.bind(this)
        this._onImageChange = this._onImageChange.bind(this)
        this._onConfirmImageURLClick = this._onConfirmImageURLClick.bind(this)
        this._onCancelImageURLClick = this._onCancelImageURLClick.bind(this)

        this.state = {
            urlFlag : false,
            urlValue: '',
            imageURL: '',
            imageFlag: false,
            imageSource: '',
            file: '',
            setTopicFlag:false,
            warningFlag:false,
        }

    }
//tab not working !!!!
    _onKeyBoardCommand(ev){
        console.log("prog control: into the keyboard loop ", ev.keyCode)
        if(ev.key === 'Tab'){ //KeyCode 9 is for Tab.
            console.log("prog control: into the TAB loop ")
            const {editorState, onMaxEditorChange} = this.props
            const newEditorState = RichUtils.onTab(ev, editorState, 4) //default space for tab is set as 4 spaces
            if (newEditorState !== editorState){
                onMaxEditorChange(newEditorState)
                console.log("prog control: into the newEditorState !== editorState loop ")
            }
            return
        }
        return getDefaultKeyBinding(ev)
    }

    _onSingleLineStyle(command){
        const {editorState, onMaxEditorChange} = this.props
        onMaxEditorChange(RichUtils.toggleInlineStyle(editorState, command));
    }
    _onMultiBlockStyle(command){
        //console.log('command is :', command)
        const {editorState, onMaxEditorChange} = this.props
        onMaxEditorChange(RichUtils.toggleBlockType(editorState, command));
    }

    _onLinkClick(e){
        e.preventDefault()
        const {editorState} = this.props
        //console.log('into the link button!!!!')
        const selection = editorState.getSelection(); //returns anchor and focus, key/offset pair
        const anchorKey = selection.getStartKey()   //returns the anchorKey of the selected content block.
        const startOffset = selection.getStartOffset()  //returns the anchorOffset of the selected content.
        const currentContent = editorState.getCurrentContent() //returns SelectionState after/before
        const currentContentBlock = currentContent.getBlockForKey(anchorKey) // get the current selected Block
        //const startPoint = currentContentBlock.getStartOffset();
        //const endPoint = currentContentBlock.getEndOffset();
        //const selectedText = currentContent.getText().slice(startPoint, endPoint); //slicing the selected Text
        //console.log('selectedText is :',currentContent.getCharacterList())
        const linkKey = currentContentBlock.getEntityAt(startOffset); //returns the key of the link entered.ie 1,2,3...
        //console.log('linkkey :', linkKey)

        let url = ''  //making url empty allow us to add new urls for other content.
        if (linkKey) {
            const linkInstance = currentContent.getEntity(linkKey);
            url = linkInstance.getData().url;
            console.log('inside the linkkey', url)
            //if already the text is pinged with an url. u remove it!!!!
            if(url){
                this.props.onMaxEditorChange(RichUtils.toggleLink(editorState, selection, null))
                return
            }
        }


        this.setState({urlFlag: true,  urlValue: url,})
    }

    _onImageClick(e){

        e.preventDefault()
        this.setState({imageFlag: true, imageURL: ''})
    }

    _onURLChange(e){
        const {urlValue} = this.state
        console.log('e.target.value is ',this.state.urlValue)
        this.setState({urlValue: e.target.value})
        //console.log('e.target.value is ',e.target.value)

    }

    _onImageChange(e){
        //const {imageURL} = this.state
        e.preventDefault()
        let imageSrc
        const file = e.target.files[0]
        const reader = new FileReader();
        //reader.readAsDataURL(file);
        console.log('e.target.value of image is ',file)
    /*    reader.onloadend = function(event) {
            //imageSrc = [reader.result]
            this.setState({imageURL: [reader.result]})
        }.bind(this)
    */    //this.setState({imageURL: [reader.result]})
        reader.onloadend = () => {
            this.setState({
                file:file,
                imageURL: reader.result
            });
        }

        reader.readAsDataURL(file)
        //console.log('e.target.value of image is ',imageSrc)
       // this.setState({imageURL: e.target.value})
        //return this.state.imageURL


    }
    _onConfirmImageURLClick(e){
        e.preventDefault();
        const {imageURL} = this.state;
        console.log('target imageUrl inside confirm click', imageURL)
        const {editorState} = this.props
        const contentState = editorState.getCurrentContent();
        const contentStateWithEntity = contentState.createEntity(
            'image',
            'IMMUTABLE',
            {src: imageURL } // height: 300, width: 300,
        );
        const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
        const newEditorState = EditorState.set(
            this.props.editorState,
            {currentContent: contentStateWithEntity}
        );
        //const entityKey = Entity.create('image', 'IMMUTABLE', {src: imageURL})
  /*      this.props.onMaxEditorChange({   //instead of this.setState
            editorState: AtomicBlockUtils.insertAtomicBlock(
                editorState,
                entityKey,
                ' '
            )})
    */
        //this.props.editorState = AtomicBlockUtils.insertAtomicBlock(newEditorState, entityKey, ' ')
        this.props.onMaxEditorChange(AtomicBlockUtils.insertAtomicBlock(newEditorState, entityKey, ' '))

        this.setState({
            imageFlag: false,
            //file:'',
            imageURL: '',


        })



    }

    _onConfirmURLClick(e){
        e.preventDefault();
        const {urlValue} = this.state;
/*           if(urlValue.startsWith('www')){
            let oldURL = urlValue
            console.log('into the special wwww looop ,before url is:', oldURL)
            this.setState((oldURL)=> {urlValue: 'http://' + oldURL})
            console.log('www after is:', this.state.urlValue)
        }
*/
        const {editorState} = this.props
        const contentState = editorState.getCurrentContent();
        const contentStateWithEntity = contentState.createEntity(
            'LINK',
            'MUTABLE',
            {
                target: '_blank',   //to open link in new tab when clicked while rendering.
                url: urlValue,      //URL to function correctly should begin with "http://"
                rel:"noopener noreferrer external"  //noopener noreferrer added to tackle against "TAB-NABBING attack.

            }
        );
        const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
        //console.log('the entity key is :', entityKey)
        const newEditorState = EditorState.set(editorState, { currentContent: contentStateWithEntity });
        this.props.onMaxEditorChange(RichUtils.toggleLink(newEditorState,
            newEditorState.getSelection(),
            entityKey))
        this.setState({urlFlag: false,
            urlValue: '',})
    }
    _onCancelURLClick(){                 //if user to come back to editor without adding link.
        return this.setState({urlFlag: false })
    }

    _onCancelImageURLClick(){
        return this.setState({imageFlag: false })
    }

    _onEnterKeyDown(ev){
        if(ev.keyCode === 13){  //13 is enter Key
            this._onConfirmURLClick(ev)
        }
    }

    enterNewTopic(){
        if(this.state.setTopicFlag){
            return(
                <div>

                    <Field name="setTopic" component="input" onChange={(e)=> {
                        newTopic.push(e.target.value)
                        //console.log('test25 input is', e)
                        console.log('/n test25 input array  is', newTopic)

                    }}/>

                </div>
            )
        }
        return

    }


    render(){
        const {
            editorState,
            handleSubmit,
            onMaxEditorChange,
        } = this.props;

        let enterURL, enterImageURL
        if(this.state.urlFlag){
            enterURL = <div>
                <input type='text' onChange={this._onURLChange} onKeyDown={this._onEnterKeyDown}/>
                <button onMouseDown={this._onConfirmURLClick} > Confirm</button>
                <button onMouseDown={this._onCancelURLClick} > Cancel</button>
            </div>
        }

        if(this.state.imageFlag){
            enterImageURL = <div>
                {/*value={this.state.imageURL}  ref="file"*/}
                <input type="file"  onChange={this._onImageChange} />
                <button onMouseDown={this._onConfirmImageURLClick} > Confirm</button>
                <button onMouseDown={this._onCancelImageURLClick} > Cancel</button>
            </div>
        }







        return(
            <div>
                <form onSubmit={handleSubmit}>

                    <div style={{margin: '10px 5px 15px 20px'}}>

                        <Field
                            name="favoriteColor"
                            component={DropdownList}
                            data={displayTopics}
                            onSelect={(e)=> {
                                console.log('test25 before',e)
                                //this.colorValue = e.color
                                colorValue.push(e)
                                if(e==='Other'){
                                    console.log('test25 after',e)
                                    this.setState({setTopicFlag:true})
                                }
                                //colorFunction(e.color)
                                console.log('test1 after',this.colorValue)
                                //this.setState({colorValue:e.color}, ()=>{console.log('test1',this.state.colorValue)})
                            }}
                            valueField='colorValue'
                                //{(colorValue)=> {
                                //console.log('test1:the color value is: ',colorValue)
                                //this.setState({colorValue})
                            //}}
                            textField="color"/>
                    </div>
                    <div style={{margin: '10px 5px 15px 20px'}}>
                    { this.enterNewTopic() }
                    </div>
                    <div style={{margin: '10px 5px 15px 20px'}}>
                        <button type="button" onMouseDown={()=> this._onSingleLineStyle('BOLD')}>Bold</button>
                        <button type="button" onMouseDown={()=> this._onSingleLineStyle('ITALIC') }>Italic</button>
                        <button type="button" onMouseDown={()=> this._onSingleLineStyle('UNDERLINE') }> Underline</button>
                        <button type="button" onMouseDown={()=> this._onSingleLineStyle('CODE') }> MonoSpace</button>
                        <button type="button" onMouseDown={()=> this._onSingleLineStyle('STRIKETHROUGH') }> StikeThrough</button>
                        <button type="button" onMouseDown={()=> this._onMultiBlockStyle('header-one') }> Header1</button>
                        <button type="button" onMouseDown={()=> this._onMultiBlockStyle('header-two') }> Header2</button>
                        <button type="button" onMouseDown={()=> this._onMultiBlockStyle('header-three') }> Header3</button>
                        <button type="button" onMouseDown={()=> this._onMultiBlockStyle('header-four') }> Header4</button>
                        <button type="button" onMouseDown={()=> this._onMultiBlockStyle('header-five') }> Header5</button>
                        <button type="button" onMouseDown={()=> this._onMultiBlockStyle('unordered-list-item') }> UL</button>
                        <button type="button" onMouseDown={()=> this._onMultiBlockStyle('ordered-list-item') }> OL </button>
                        <button type="button" onMouseDown={()=> this._onMultiBlockStyle('blockquote') }> Quote</button>
                        <button type="button" onMouseDown={()=> this._onMultiBlockStyle('code-block') }> CodeBlock</button>
                        <button type="button" onMouseDown={this._onLinkClick}> Link</button>
                        <button type="button" onMouseDown={this._onImageClick}> Add Image</button>

                        {enterURL}
                        {enterImageURL}





                        <Editor
                            editorState={editorState}
                            onChange={onMaxEditorChange}
                            spellCheck={true}
                            keyBindingFn={this._onKeyBoardCommand}
                            blockRendererFn={mediaBlockRenderer}

                        />

                    </div>


                    <button type="submit">Post Content</button>

                </form>

            </div>
        )
    }
}
MaxEditor.propTypes = {
    editorState: PropTypes.object.isRequired,
    onMaxEditorChange: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    colorValue: PropTypes.object.isRequired
}
const MaxEditorForm = reduxForm({
    form: 'editorform'
})(MaxEditor)


class MaxEditorSubmit extends Component{
    constructor(props){
        super(props)
        //console.log("test1 totalTOpics in subm",this.props.totalTopics)
        displayTopics = this.props.totalTopics //an array with all the topicName from topic_document
        displayTopics.push('Other') //for user intended option was'nt available, user can opt for "Other"
        const decorator = new CompositeDecorator([
            {
                strategy: findLinkEntities,
                component: Link,
            },
        ]);

        this.state = {editorState: EditorState.createEmpty(decorator)};
        this.handleEditorChange = this.handleEditorChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    handleEditorChange(editorState) {
        this.setState({editorState});
    }
    handleSubmit(){
        const {post_article} = this.props   /* calling action handler*/
        //const colorValue = this.props
        const editorState = this.state.editorState;
        const contentState = editorState.getCurrentContent();
        const html = stateToHTML(contentState)  //html is the content written
        let postedTopic = colorValue[colorValue.length -1]
        let setTopic = newTopic[newTopic.length -1]
        console.log('test25; testing the condition before', colorValue, newTopic)
        if(postedTopic === 'Other'){
            console.log('test25; testing the condition after ', postedTopic, setTopic)
            postedTopic = setTopic
        }

        console.log('test1 html from form is: ', html,postedTopic)
        if(html==="<p><br></p>" || postedTopic === undefined) {
            console.log('test26 html from form is: ', html,postedTopic)
            //this.setState({warningFlag:true}, ()=> {console.log('flag changed')})
            window.alert('Cannot Summit article, Missing content or topic!!')

        }else {
            post_article({writtenContent:html, writtenTopic:postedTopic})
            this.props.history.push('/home')
        }


    }
    render(){
        return(
            <MaxEditorForm
                editorState={this.state.editorState}
                onMaxEditorChange={this.handleEditorChange}
                onSubmit={this.handleSubmit}
            />
        )
    }
}
const styles = {
    forUL: {
        color: '#090',
        cursor: 'pointer',
        marginRight: 16,
        padding: '2px 0',
    },
};


MaxEditorSubmit.propTypes = {
    post_article: PropTypes.func.isRequired,
};

function findLinkEntities(contentBlock, callback, contentState) {
    contentBlock.findEntityRanges(
        (character) => {
            const entityKey = character.getEntity();
            return (
                entityKey !== null &&
                contentState.getEntity(entityKey).getType() === 'LINK'
            );
        },
        callback
    );
}
const Link = (props) => {
    const {url} = props.contentState.getEntity(props.entityKey).getData();
    console.log('the final url is:', url)
    return (
        <a href={url} style={styles.link}  >
            {props.children}
        </a>
    );
};
function mediaBlockRenderer(block) {
    if (block.getType() === 'atomic') {
        return {
            component: Image,
            editable: false,
            //editable: true,
        };
    }
    return null;
}

const Image = (props) => {

    const entity = props.contentState.getEntity(
        props.block.getEntityAt(0)
    );
    const {src} = entity.getData();
    console.log('program control passed to image block render function', src)
    return <img src={src}  />;
    //return <h2>{props.src}</h2>
};

export default connect(null,{post_article})(withRouter(MaxEditorSubmit));