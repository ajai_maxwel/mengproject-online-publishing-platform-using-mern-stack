import React, {Component} from 'react'
import {connect} from 'react-redux'
import {view_Content, post_Likes, post_Report, post_Comment} from "../actions";
import {Link} from 'react-router-dom'
import { Field, reduxForm } from 'redux-form'

class ViewContent extends Component{
    constructor(props){
        super(props)
        this._onLikeClick = this._onLikeClick.bind(this)
        this.state = {likes: 0, likeFlag:true, postID:''}
        //this.likeCount = this.likeCount.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.showComment = this.showComment.bind(this)
        this.updateLikes = this.updateLikes.bind(this)
    }
    componentDidMount(){
        const p = this.props.history.location.pathname.split('/')
        //const p1 = JSON.parse(p[p.length -1])
        console.log('pathname is : ',p[2])
        this.setState({postID:p[2]})

        //this.props.view_Content(p[p.length -1] )
        this.props.view_Content(p[2] )
    }
    /*
    * Like Feature: an user can click as many likes as possible
    */
    _onLikeClick(e,postID){
        e.preventDefault()
        this.props.post_Likes({postID})
    }
    updateLikes(postID){
        let likeCount = this.state.likes
    }



    onReportClick(postID){
        this.props.post_Report({postID})
    }
    handleSubmit(e){
        //e.preventDefault()
        //console.log('comment: ',this.state.postID)
        const comment = e.target.comment.value
        const postID = this.state.postID
        this.props.post_Comment({comment, postID})
    }

    commentForm({postID}){
        return <div>
            <form onSubmit={this.handleSubmit}>
                <label>Comment</label>
                <Field name="comment" component="textarea"/>
                <button type="submit">post</button>
            </form>
        </div>
    }

    showComment(post){
        console.log('testing comment:',post)
        if(post.comments.length){
            console.log('testing comment2:',post.comments)
            //let oldArray = post.comments
            //oldArray.sort((a,b)=> a.postedOn-b.postedOn)
            return post.comments.reverse().map(comment => {
                return(
                    <div className="card white darken-4 hoverable">
                        <div className="card-content" >
                            <li>{comment.comment}</li>
                            <span><small>PostedBy: {comment.postedBy}</small></span>
                            <span><small>PostedOn: {comment.postedOn}</small></span>
                        </div>
                    </div>
                )
            })
        }
    }

    renderContent(){
        return this.props.Post.map(post => {
            return(
                <div className="container"  >
                    <div className="card white darken-2 hoverable">
                        <div className="card-content" >
                            <li key={post._id} dangerouslySetInnerHTML = {{ __html: post.postContent }} />

                            {/*Button for Like*/}
                            <button key={21} type='button' className="btn rounded" onMouseDown={(e) => this._onLikeClick(e,post._id)}>
                                👏 {post.likes}
                            </button>
                            <div className="waves-effect waves-light btn-small right materialize-red-text" style={{margin: '10px 10px'}}>
                                <button onMouseDown={(e)=>this.onReportClick(post._id)}>Report Abuse</button>
                            </div>

                        </div>
                        {/* link to main page*/}
                        <Link to="/"> Main page</Link>

                        {this.commentForm(post._id)}
                        <div>
                            <ul>
                                {this.showComment(post)}
                            </ul>
                        </div>
                    </div>
                </div>
            )
        })
    }

    render(){
        return(
            <div>
                <ul>
                {this.renderContent()}
                </ul>
            </div>
        )
    }




}

function mapStateToProps(state){
    console.log("state_post of view content is: ",state.open)
    return {Post: state.open}  //from reducer
}

ViewContent = reduxForm({
    form: 'commentform',
})(ViewContent)

export default connect(mapStateToProps,{view_Content, post_Likes, post_Report, post_Comment})(ViewContent)