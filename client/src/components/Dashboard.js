import React, {Component} from 'react'
import {connect} from 'react-redux'
import {get_Posts} from '../actions'
import {pin_Post} from '../actions'
//import {post_Likes} from '../actions'
//import * as actions from '../actions'
import 'materialize-css/dist/css/materialize.min.css'
import '../style/index.css'

class Dashboard extends Component {
    constructor(props){
        super(props)
        this.renderSurveys = this.renderSurveys.bind(this)
        this.state = {Q:0}
    }
    componentWillReceiveProps() {
        this.externalRender = true;
    }

    componentDidMount(){
        this.props.get_Posts()
        window.addEventListener('scroll', (event) => {
            let scrollHeight = window.scrollY     //window.scrollY //event.srcElement.body.scrollHeight : 2821
            let roundQ = Math.round(window.scrollY/192)   //let roundQ = Math.round(window.scrollY/192)
            this.setState({Q: roundQ},()=> {
                console.log('rounded q is: ',roundQ)
                //this.renderSurveys(this.state.Q,this.state.Q+20,this.props)
           })
            //window.scrollY: 2238, document.documentElement.scrollTop:2238
    })

    }

//window.innerHeight: 615
    handleScroll(){
        console.log('scroll',document.body.scrollHeight)
    }
/*
* Infinite Schrolling
* my window.innerHeight: 615, here we are able to see the header plus 3 post-snippet
* ratio can be assumed to be 615:4 = 154px for one snippet
* 16 px = 1 em, In Dashboard height of 1 post_snippet is 8.4 em = 134.4 px
* 2238,
* */

    renderSurveys(a,b,props){
        return this.props.Post.reverse().slice(0,b).map(post => {
            console.log('window: inside return: ', a,b)
            //finding window dimensions// window.innerHeight: 615,
            //console.log('window:height of the window is:',window.innerHeight,'size of one post is :',document.body.scrollHeight, document.body.scrollHeight)
            return(
                    <li key={post._id}    className="card white darken-1 hoverable">

                      <div className="card-content">
                        <div className="text ellipsis">
                            <a href={`/post/${post._id}`}  target="_blank">
                            <span className="card-title text-concat">
                                {/* post.postContent */}
                                {/*-- to make use of rich text  dangerouslySetInnerHTML is used */}
                                <div dangerouslySetInnerHTML={{ __html: post.postContent }} />
                            </span>
                            </a>
                        </div>

                            <p><small>posted by: {post.postedBy}</small></p>
                            <p><small>posted on: {post.postedOn}</small></p>


                            <button style={{margin: '5px 5px'}}  onClick={pin_Post(post).bind(this)}>Pin It
                                <i className="material-icons">class</i>
                            </button>
                            <button type='button' className="new badge light-blue" style={{margin: '5px 5px'}}>
                                Claps: {post.likes}
                            </button>
                              {/*
                              <button type='button' onMouseDown={()=> this.props.post_Likes(post._id)}>Claps {post.likes}</button>

                              <LikeButton postID={post._id}/>
                              passing the post_ID to the like component to fetch like details
                              */}
                    </div>
                </li>

            )
        })
    }
    render(){
        return(
            <div>
                <ul>
                    {this.renderSurveys(this.state.Q,this.state.Q+10, this.props.Post)}
                </ul>
            </div>


        )

    }
}


function mapStateToProps(state){
    console.log("state_post is ",state.post)
    return {Post: state.post}  //from reducer
}

export default connect(mapStateToProps,{get_Posts, pin_Post})(Dashboard)
//export default connect(mapStateToProps,actions)(Dashboard)





