import React, {Component} from 'react'
import {get_Reports, ban_User, delete_Post, delete_Report} from "../actions";
import {connect} from 'react-redux'
import { Link } from 'react-router-dom';
import ShowReport from './Forms/ShowReport'

class AdminPortal extends Component{
    constructor(props){
        super(props)
        this.state={re:'', showFlag:false, total:this.props.Reports.length}
    }

    componentDidMount(props){
        this.props.get_Reports()
    }
    onClickBan(e, userID){
        e.preventDefault()
        console.log('ban click!!', userID)
        this.props.ban_User({userID})
    }
    onViewClick({content}){
        console.log('passed content is: ',content)
        let la = <div dangerouslySetInnerHTML = {{ __html: content }} />
       this.setState({re:la, showFlag:true})
    }

    viewContent(){
        if(this.state.showFlag)
            return <div>
                {this.state.re}
                <div><button onMouseDown={()=>this.setState({showFlag:false})}>Close</button></div>
            </div>
        return
    }

    onClickDeletePost(e, postID){
        e.preventDefault()
        console.log('delete post click!!', postID)
        this.props.delete_Post({postID})
    }
    onClickEntryDelete(e, reportID){
        //e.preventDefault()
        console.log('delete report click!!', reportID)
        this.props.delete_Report({reportID})
        this.setState({total: this.state.total-1})
    }
    renderReports(){
        return this.props.Reports.map(report => {
            return(

                      <tr key={report._id}>
                              <td>{report.name}</td>
                              <td>{report.googleID}</td>
                              <td>
                                  <button className="btn waves-effect waves-light btn-small" type="button" onMouseDown={(e)=>this.onViewClick({content:report.articleReportedContent})}>View Content</button>
                              </td>
                              <td>{report.votes}</td>
                              <td>
                                  <button disabled= {report.deleteFlag} className="btn waves-effect waves-light btn-small" type='button' onMouseDown={(e)=>this.onClickDeletePost(e,report.articleReportedID)}>
                                      Delete Post
                                  </button></td>
                              <td>
                                  <button  className="btn waves-effect waves-light btn-small" type='button' onMouseDown={(e)=>this.onClickBan(e,report.googleID)}>
                                      {report.banFlag? 'Block':'UnBlock'}
                                  </button></td>
                              <td>
                                  <button className="btn waves-effect waves-light btn-small" type='button' onMouseDown={(e)=>this.onClickEntryDelete(e,report._id)}>
                                      remove field
                                  </button>
                              </td>

                      </tr>

            )
        })
    }

    render(){
        return(
            <div className="container" style={{width:'1000px'}}>
                <h4>List of reported users..</h4>
                <span>No of reports {this.props.Reports.length}</span>
                <div style={{tableLayout:'fixed' ,height: '300px',overflowX:'scroll', overflowY : 'scroll'}} >
                    <table className="table-of-contents responsive-table striped "   >
                        <thead>
                        <tr>

                            <th>User Name</th>
                            <th>UserID</th>
                            <th>Postcontent</th>
                            <th>No of votes</th>
                            <th>Remove post</th>
                            <th>Block User</th>
                            <th>Remove</th>
                        </tr>
                        </thead>
                        <tbody >
                        {this.renderReports()}
                        </tbody>


                    </table>

                </div>

                {this.viewContent()}
            </div>
        )
    }

}

function mapStateToProps(state){
    console.log("state_post is ",state.reports)
    return {Reports: state.reports}  //from reducer
}

export default connect(mapStateToProps,{get_Reports, ban_User, delete_Post, delete_Report})(AdminPortal)