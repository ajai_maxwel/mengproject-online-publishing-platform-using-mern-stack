/*
*Component: SearchBar
* 1. Make a input bar for writing text to search
* 2. A button to submit the search.
* 3. Make an action handler to send request to BackEnd
*Component: SearchResults
* 1.Set an action handler to retrieve results from Store.
* 2.Display the result.
*
*
* */
import React, {Component} from 'react'
import {request_Search} from "../actions/index";
import {connect} from 'react-redux'
import * as actions from '../actions'


class SearchBar extends Component {
    constructor(){
        super()
        this._onSearchChange = this._onSearchChange.bind(this)
        this._onSearchClick = this._onSearchClick.bind(this)
        this.state = {
            searchString: '',
        }
    }
    _onSearchChange(e){
        this.setState({searchString:e.target.value})
        console.log('the search string is: ', e.target.value)
    }

    _onSearchClick(e){
        e.preventDefault()
        this.props.request_Search(this.state.searchString)
    }


    render(){
        return(
            <div className="container">

                <input type="text"  onChange={this._onSearchChange}/>
                <button type="button" onMouseDown={this._onSearchClick}>Search</button>

            </div>
        )
    }
}

export default connect(null,{request_Search})(SearchBar)