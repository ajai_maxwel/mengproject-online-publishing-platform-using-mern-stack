import React from 'react'
import {Field, reduxForm} from 'redux-form'
import Multiselect from 'react-widgets/lib/Multiselect'

const TopicForm = props => {
    const {handleSubmit} = props
    const renderMultiselect = ({ input, data, valueField, textField }) =>
        <Multiselect {...input}
                     onBlur={() => input.onBlur()}
                     value = {input.value || this.props.auth.topicInterested}  //value={input.value || ['hello', 'maxwel']} requires value to be an array
                     data={data}
                     valueField={valueField}
                     textField={textField}
        />

    return(
        <form onSubmit={handleSubmit}>
            <div className="">
                <label>Choose your interested topics</label>
                <Field name="topicList" component={this.renderMultiselect} data={this.renderTopics()}/>
            </div>

            <button type="submit" >Done</button>


        </form>
    )
}
export default reduxForm({form: 'topicform'})(TopicForm)