import React, {Component} from 'react'
import {get_Pins} from "../actions";
import {connect} from 'react-redux'

class Home extends Component {

    componentDidMount(){
        this.props.get_Pins()
        //console.log("this.props.get_Pins",this.props)
    }
    render(){

        return(

            <div>
                {this.props.Post.map(post=>{

                    return(
                        <div className="card white darken-4 hoverable">
                            <a href={`/post/${post._id}` }  target="_blank">
                            <div class="card-content">
                                <div>
                                    {/* <p>{post.postContent}</p>  */}
                                    {/*-- to make use of rich text  dangerouslySetInnerHTML is used */}
                                    <div dangerouslySetInnerHTML={{ __html: post.postContent }} />
                                </div>
                            </div>
                            </a>
                        </div>
                    )
                })}
            </div>
        )
    }
}
function mapStateToProps(state){
    console.log("state_post is ",state.post)
    return {Post: state.pin}  //from reducer
}

export default connect(mapStateToProps,{get_Pins})(Home)