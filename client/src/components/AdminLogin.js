import React, {Component} from 'react'
import { Field, reduxForm } from 'redux-form'
import {admin_Validate, get_Admin} from '../actions'
import {connect} from 'react-redux'
import 'materialize-css'
import AdminPortal from './AdminPortal'

class AdminLogin extends Component{

    constructor(props){
        super(props)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.renderPortal = this.renderPortal.bind(this)
        this.state ={warningFlag:false}
    }
    componentDidMount(){
        this.props.get_Admin()
    }
    handleSubmit(e) {
        e.preventDefault()
        let sqlInject = RegExp(/[$-/:-?{-~!="^'_`\[\]]/)
        //sqlInject has the list of symbols that could be tried by malicious user for sql injection.
        let userName = e.target.userName.value
        let password = e.target.password.value
        console.log('admin:the values obtained on handle submit is: ', e.target.userName.value, e.target.password.value)
        if(!sqlInject.test(userName)){
            if(!sqlInject.test(password)){
                console.log('sqlinject test: ', userName, password)
                this.props.admin_Validate({userName, password})
            }
        }
        //this.setState({warnFlag:true})



    }
    warningMessage(){
        if(this.state.warnFlag){
            return(
                <div><span style={{color:'red'}}>**We are safe against sql Injection!!!Not the right entry point to exploit </span></div>
            )
        }
    }

    renderPortal(){
        console.log('test02: admin props ', this.props.Admin)
        if(this.props.Admin){

            return <div>
                <div>Admin Name: {this.props.Admin.username}</div>
                <AdminPortal />
            </div>
        }
        return<div>
            <div className="container black-text">
                <div>Enter credentials</div>
                <form onSubmit={this.handleSubmit}>
                    <div>
                        <label>UserName</label>
                        <Field
                            name="userName"
                            component="input"
                            type="text"
                            placeholder="enter userName"
                        />
                    </div>
                    <div>
                        <label>Password</label>
                        <Field
                            name="password"
                            component="input"
                            type="password"
                            placeholder="enter password"
                        />
                    </div>
                    <div>
                        <button type="submit">Login</button>
                    </div>
                </form>
            </div>
            {this.warningMessage()}

        </div>


    }
    render(){
        return(
            <div>
                {this.renderPortal()}
            </div>
              )
    }
}

AdminLogin = reduxForm({
    form: 'adminloginform',
})(AdminLogin)

function mapStateToProps(state){
    console.log("test02:state_post is ",state.admin)
    return {Admin: state.admin}  //from reducer
}

export default connect(mapStateToProps, {admin_Validate, get_Admin})(AdminLogin)