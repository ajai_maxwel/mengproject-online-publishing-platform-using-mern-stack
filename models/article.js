const mongoose = require('mongoose')
const {Schema} = mongoose
const commentSchema = require('./comment')


const articlesSchema = new Schema({
    postContent: String,
    postedBy: String,
    likes:{type:Number, default: 0},
    dislikes:{type:Number, default: 0},
    postedOn:{ type: Date, default: Date.now },
    comments:[commentSchema],
    })
articlesSchema.index({'$**': 'text'})  //now it searches all the fields.
//if needed to search particular field, this can be modified to EX: {name: 'text', 'profile.something': 'text'}
mongoose.model('articles', articlesSchema)
