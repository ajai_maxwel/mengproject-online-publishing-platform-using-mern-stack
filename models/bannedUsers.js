const mongoose = require('mongoose')
const {Schema} = mongoose



const bannedUsersSchema = new Schema({
    googleID: String,
    name: String,
    dayBanned: Date,
})
mongoose.model('bannedUsers', bannedUsersSchema)
