const mongoose = require('mongoose')
const {Schema} = mongoose
const mailSchema = require('./mail')

const topicSchema = new Schema({
    topicName: String,
    mailList: [mailSchema]
})
mongoose.model('topics', topicSchema)