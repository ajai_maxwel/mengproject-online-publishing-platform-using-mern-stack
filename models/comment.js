const mongoose = require('mongoose')
const { Schema } = mongoose

const commentSchema = new Schema({
        comment : String,
        postedBy: String,
        userID : String,
        postedOn:Date,
    }

)
module.exports = commentSchema