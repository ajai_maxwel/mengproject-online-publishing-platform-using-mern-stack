const mongoose = require('mongoose')
const {Schema} = mongoose


const userSchema = new Schema({
    googleID: String,
    name: String,
    email: String,
    topicInterested: String,
    PinnedPost: String,
    subscribeFlag: {type:Boolean, default:false},
    status:{type:Boolean, default:true}, //true is active
})
mongoose.model('users', userSchema)
