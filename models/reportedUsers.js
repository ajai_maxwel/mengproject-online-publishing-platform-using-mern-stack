const mongoose = require('mongoose')
const {Schema} = mongoose



const reportsSchema = new Schema({
    googleID: String,
    name: String,
    email: String,
    articleReportedID: String,
    articleReportedContent: String,
    votes:Number,
    banFlag:{type:Boolean, default:true},    //true indicating user is active
    deleteFlag:{type:Boolean, default:false} //post delete
})
mongoose.model('reports', reportsSchema)
