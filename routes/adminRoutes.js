const mongoose = require('mongoose')
mongoose.connect('mongodb://admin04:admin04@ds125578.mlab.com:25578/mengprojectdb')
require('../models/adminCredentials')
require('../models/article')
require('../models/reportedUsers')
const Admin = mongoose.model('admin')
const Articles = mongoose.model('articles')
const Reports = mongoose.model('reports')
var sanitize = require('mongo-sanitize');

module.exports = app => {
    //save new admin
    console.log('into adminroutes')
    app.post('/admin/new', async (req, res) => {
        console.log('into the post new admin group!')
        const hisUsername = 'admin04'
        const hispassword = 'zaq1!2@wsx'

        const newAdmin = new Admin({
            username: hisUsername,
            password: hispassword
        })
        await newAdmin.save()
        res.send('Admin saved')

    })

    //validate the admins credentials. '/api/admin/validate'
    app.post('/api/admin/validate', async (req,res)=> {

        const {userName,password} = (req.body)
        console.log('into the admin validation loop!',userName,password)
        const validate = await Admin.findOne({username: userName}, function (err, user) {
                if (err) throw console.log('invalid username');

                // test a matching password
                if(user)
                 user.comparePassword(password, function (err, isMatch) {
                    if (err) throw console.log('invalid username');
                    if(isMatch){
                        console.log('Password:', isMatch);

                        res.send(user)
                    }
                });

            })

    })

    //delete post ..'/api/admin/delete/post
    app.post('/api/admin/delete/post', async (req,res)=> {
        console.log('in delete post , postid is', req.body)
        const {postID} = req.body
        const deletePost = Articles.remove({_id: postID})
        await deletePost.exec()
        const setFlag = Reports.update({articleReportedID:postID}, {deleteFlag:true})
        await setFlag.exec()
        const reports = await Reports.find({})
        res.send(reports)
    })
    //to delete a report document
    app.post('/api/admin/delete/report', async (req,res)=> {
        console.log('in delete post , postid is', req.body)
        const {reportID} = req.body
        const deleteReport =  Reports.remove({_id: reportID})
        await deleteReport.exec()
        const reports = await Reports.find({})
        res.send(reports)
    })
}