module.exports = (req, res, next) => {
    if(!req.user){   //to make sure user is logged in. Ex: while using a link to access application.
        return res.status(401).send({error:'You must login!!'})
    }
    next()
}