const keys = require('../config/keys')

const sendgrid = require('sendgrid')
const helper = sendgrid.mail
const mailTemplate = require('./mailTemplate')

//coded as normal react component.
class SendMail extends helper.Mail{
    constructor(mailRecipients, postID, topicName){
        super()
        this.sgApi = sendgrid(keys.SENDGRID_KEY)
        this.from_email = new helper.Email('no-reply@mengproj.com')
        this.subject = 'Checkout the new article on ' + topicName
        let content  = mailTemplate(postID,topicName)
        this.body = new helper.Content('text/html', content)
        this.recipients = this.formatAddresses(mailRecipients)

        this.addContent(this.body)  //by Mail
        this.addClickTracking()     //by us
        this.addRecipients()        //by us

    }



    formatAddresses(mailRecipients){
        console.log('mailrecipents array is: ', mailRecipients)
        return mailRecipients.mailList.map(
            ({mail}) => {
                console.log('mail is: ',mail)
                return new helper.Email(mail)


            })
    }
    addClickTracking(){
        const trackingSettings = new helper.TrackingSettings()
        const clickTracking = new helper.ClickTracking(true, true)
        //this is how send grid works
        trackingSettings.setClickTracking(clickTracking)
        this.addTrackingSettings(trackingSettings)
    }

    addRecipients(){
        const personalize = new helper.Personalization
        this.recipients.forEach(recipient =>{
            personalize.addTo(recipient)
        })
        this.addPersonalization(personalize)
    }

    async send(){
        const request = this.sgApi.emptyRequest({
            method: 'POST',
            path: '/v3/mail/send',
            body: this.toJSON(), //by Mail

        })

        const response = await this.sgApi.API(request)
        return response

    }
}


module.exports = SendMail