Objective: 
The objective of the project is to create a space where insightful writers, narrators and thinkers can share their experiences, write tutorials, explain new hypothesis etc. adding value to the community in terms of knowledge sharing. The project is implemented based on REST API using one of the dominant JavaScript web app stack- MERN (MongoDB, Express, React, Node.js).

Technologies used: 
React, Redux, Node.js, MongoDB, Draft.js, Axios, SendGrid API, OAuth API.

(refer the documentation folder for the project report)

Functional Description:
•	The application, provides users an elegant rich text editor to assist in writing articles. Any user who has signed up for the application is authorized to write content. 

•	User can respond to an article by posting comments and likes.

•	Every user is provided with his own board wherein he can pin an article and it will be available as long as he intends to keep it. This feature can come in handy when a user finds an article to be interesting and want to read it again or keep it as a reference or wants to read later. 

•	To ensure the value the site holds, we also have features that enable users to report any irrelevant or defaming post. 

•	Also, the space enables users to choose his topic of interest and to subscribe for it. So, whenever a new article which is related to his subject of interest is posted we notify users by sending mail with the new article link. 

•	Apart from the application has a search bar where users can search any topic of interest. 

•	It’s important to keep the website away from spammers. We do have an admin panel, wherein admin can see the reported post, judge/review the content and take appropriate actions. Admins are enabled to delete the reported content. Additionally, they can block the user from accessing the page. Admin also have the capability to unblock the blocked users.

